package com.reeco.terminal.controller;

import java.util.List;

import com.reeco.terminal.model.Indicator;
import com.reeco.terminal.model.NumericSeries;
import com.reeco.terminal.service.TerminalService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TerminalController {

    @Autowired
    private TerminalService terminalService;

    @GetMapping("/test")
    public String test() {
        return "Hello";
    }

    @GetMapping("/indicator/get/{id}")
    public ResponseEntity<List<Indicator>> getIndicator(@PathVariable("id") Long id) {
        return terminalService.getIndicator(id);
    }

    @PostMapping("/indicator/add")
    public ResponseEntity<Indicator> addIndicator(@RequestBody Indicator indicator) {
        return terminalService.addIndicator(indicator);
    }

    @GetMapping("series/get/{id}")
    public ResponseEntity<List<NumericSeries>> getSeries(@PathVariable("id") Long id) {
        return terminalService.getSeries(id);
    }

    @PostMapping("series/add")
    public ResponseEntity<NumericSeries> addSeries(@RequestBody NumericSeries numericSeries) {
        return terminalService.addSeries(numericSeries);
    }

    @GetMapping("params/add/add")
    public ResponseEntity<Integer> addNewParam(
        @RequestParam("name") String name,
        @RequestParam("id") Long id,
        @RequestParam("param1") Long param1,
        @RequestParam("param2") Long param2
    ) {
        return terminalService.addNewParam(name, id, param1, param2);
    }
}
