package com.reeco.terminal.model;

import java.time.LocalDateTime;

import org.springframework.data.cassandra.core.mapping.Column;
import org.springframework.data.cassandra.core.mapping.PrimaryKey;
import org.springframework.data.cassandra.core.mapping.Table;

@Table("indicators")
public class Indicator {

    @PrimaryKey("indicator_id")
    private Long indicatorId;

    @Column("indicator_name")
    private String indicatorName;

    @Column("indicator_name_vi")
    private String indicatorNameVi;

    @Column("standard_unit")
    private String standardUnit;

    @Column("group_id")
    private Long groupId;

    @Column("value_type")
    private ValueType valueType;

    @Column("created_at")
    private LocalDateTime createdAt;

    @Column("updated_at")
    private LocalDateTime updatedAt;


    public Indicator() {
    }


    public Indicator(Long indicatorId, String indicatorName, String indicatorNameVi, String standardUnit, Long groupId, ValueType valueType, LocalDateTime createdAt, LocalDateTime updatedAt) {
        this.indicatorId = indicatorId;
        this.indicatorName = indicatorName;
        this.indicatorNameVi = indicatorNameVi;
        this.standardUnit = standardUnit;
        this.groupId = groupId;
        this.valueType = valueType;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
    }

    public Indicator(Long indicatorId, String indicatorName) {
        this.indicatorId = indicatorId;
        this.indicatorName = indicatorName;
    }

    public Long getIndicatorId() {
        return this.indicatorId;
    }

    public void setIndicatorId(Long indicatorId) {
        this.indicatorId = indicatorId;
    }

    public String getIndicatorName() {
        return this.indicatorName;
    }

    public void setIndicatorName(String indicatorName) {
        this.indicatorName = indicatorName;
    }

    public String getIndicatorNameVi() {
        return this.indicatorNameVi;
    }

    public void setIndicatorNameVi(String indicatorNameVi) {
        this.indicatorNameVi = indicatorNameVi;
    }

    public String getStandardUnit() {
        return this.standardUnit;
    }

    public void setStandardUnit(String standardUnit) {
        this.standardUnit = standardUnit;
    }

    public Long getGroupId() {
        return this.groupId;
    }

    public void setGroupId(Long groupId) {
        this.groupId = groupId;
    }

    public ValueType getValueType() {
        return this.valueType;
    }

    public void setValueType(ValueType valueType) {
        this.valueType = valueType;
    }

    public LocalDateTime getCreatedAt() {
        return this.createdAt;
    }

    public void setCreatedAt(LocalDateTime createdAt) {
        this.createdAt = createdAt;
    }

    public LocalDateTime getUpdatedAt() {
        return this.updatedAt;
    }

    public void setUpdatedAt(LocalDateTime updatedAt) {
        this.updatedAt = updatedAt;
    }    

}
