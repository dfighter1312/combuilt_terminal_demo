package com.reeco.terminal.model;

import java.time.LocalDate;
import java.time.LocalDateTime;

import javax.persistence.Convert;

import com.reeco.terminal.utils.LocalDateTimeAttributeConverter;

import org.springframework.data.cassandra.core.mapping.Column;
import org.springframework.data.cassandra.core.mapping.PrimaryKey;
import org.springframework.data.cassandra.core.mapping.Table;

@Table("numeric_series_by_organization")
public class NumericSeries {

    @Column("alarm_id")
    private Long alarmId;

    @Column("alarm_type")
    private String alarmType;

    @Column("connection_id")
    private Long connectionId;

    private LocalDate date;

    @Column("event_time")
    @Convert(converter = LocalDateTimeAttributeConverter.class)
    private LocalDateTime eventTime;

    @Column("indicator_id")
    private Long indicatorId;

    @Column("indicator_name")
    private String indicatorName;

    @Column("is_alarm")
    private Boolean isAlarm;

    private Double lat;
    
    private Double lon;

    @Column("max_value")
    private String maxValue;

    @Column("min_value")
    private String minValue;

    @PrimaryKey("organization_id")
    private Long organizationId;

    @Column("param_id")
    private Long paramId;

    @Column("param_name")
    private String paramName;

    @Column("received_at")
    private LocalDateTime receivedAt;

    @Column("sent_at")
    private LocalDateTime sentAt;

    @Column("station_id")
    private Long stationId;

    private Double value;

    @Column("workspace_id")
    private Long workspaceId;


    public NumericSeries() {
    }


    public NumericSeries(Long alarmId, String alarmType, Long connectionId, LocalDate date, LocalDateTime eventTime, Long indicatorId, String indicatorName, Boolean isAlarm, Double lat, Double lon, String maxValue, String minValue, Long organizationId, Long paramId, String paramName, LocalDateTime receivedAt, LocalDateTime sentAt, Long stationId, Double value, Long workspaceId) {
        this.alarmId = alarmId;
        this.alarmType = alarmType;
        this.connectionId = connectionId;
        this.date = date;
        this.eventTime = eventTime;
        this.indicatorId = indicatorId;
        this.indicatorName = indicatorName;
        this.isAlarm = isAlarm;
        this.lat = lat;
        this.lon = lon;
        this.maxValue = maxValue;
        this.minValue = minValue;
        this.organizationId = organizationId;
        this.paramId = paramId;
        this.paramName = paramName;
        this.receivedAt = receivedAt;
        this.sentAt = sentAt;
        this.stationId = stationId;
        this.value = value;
        this.workspaceId = workspaceId;
    }

    public NumericSeries(LocalDateTime eventTime, String indicatorName, Double value) {
        this.eventTime = eventTime;
        this.indicatorName = indicatorName;
        this.value = value;
    } 

    public Long getAlarmId() {
        return this.alarmId;
    }

    public void setAlarmId(Long alarmId) {
        this.alarmId = alarmId;
    }

    public String getAlarmType() {
        return this.alarmType;
    }

    public void setAlarmType(String alarmType) {
        this.alarmType = alarmType;
    }

    public Long getConnectionId() {
        return this.connectionId;
    }

    public void setConnectionId(Long connectionId) {
        this.connectionId = connectionId;
    }

    public LocalDate getDate() {
        return this.date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public LocalDateTime getEventTime() {
        return this.eventTime;
    }

    public void setEventTime(LocalDateTime eventTime) {
        this.eventTime = eventTime;
    }

    public Long getIndicatorId() {
        return this.indicatorId;
    }

    public void setIndicatorId(Long indicatorId) {
        this.indicatorId = indicatorId;
    }

    public String getIndicatorName() {
        return this.indicatorName;
    }

    public void setIndicatorName(String indicatorName) {
        this.indicatorName = indicatorName;
    }

    public Boolean isIsAlarm() {
        return this.isAlarm;
    }

    public Boolean getIsAlarm() {
        return this.isAlarm;
    }

    public void setIsAlarm(Boolean isAlarm) {
        this.isAlarm = isAlarm;
    }

    public Double getLat() {
        return this.lat;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public Double getLon() {
        return this.lon;
    }

    public void setLon(Double lon) {
        this.lon = lon;
    }

    public String getMaxValue() {
        return this.maxValue;
    }

    public void setMaxValue(String maxValue) {
        this.maxValue = maxValue;
    }

    public String getMinValue() {
        return this.minValue;
    }

    public void setMinValue(String minValue) {
        this.minValue = minValue;
    }

    public Long getOrganizationId() {
        return this.organizationId;
    }

    public void setOrganizationId(Long organizationId) {
        this.organizationId = organizationId;
    }

    public Long getParamId() {
        return this.paramId;
    }

    public void setParamId(Long paramId) {
        this.paramId = paramId;
    }

    public String getParamName() {
        return this.paramName;
    }

    public void setParamName(String paramName) {
        this.paramName = paramName;
    }

    public LocalDateTime getReceivedAt() {
        return this.receivedAt;
    }

    public void setReceivedAt(LocalDateTime receivedAt) {
        this.receivedAt = receivedAt;
    }

    public LocalDateTime getSentAt() {
        return this.sentAt;
    }

    public void setSentAt(LocalDateTime sentAt) {
        this.sentAt = sentAt;
    }

    public Long getStationId() {
        return this.stationId;
    }

    public void setStationId(Long stationId) {
        this.stationId = stationId;
    }

    public Double getValue() {
        return this.value;
    }

    public void setValue(Double value) {
        this.value = value;
    }

    public Long getWorkspaceId() {
        return this.workspaceId;
    }

    public void setWorkspaceId(Long workspaceId) {
        this.workspaceId = workspaceId;
    }    

}
