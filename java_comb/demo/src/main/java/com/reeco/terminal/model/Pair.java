package com.reeco.terminal.model;

public class Pair {

    private String key;
    private String value;
    private String opt;

    public Pair(String key, String value, String opt) {
        this.key = key;
        this.value = value;
        this.opt = opt;
    }

    public String getKey() {
        return this.key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getValue() {
        return this.value;
    }

    public void setValue(String value) {
        this.value = value;
    }


    public String getOpt() {
        return this.opt;
    }

    public void setOpt(String opt) {
        this.opt = opt;
    }

}
