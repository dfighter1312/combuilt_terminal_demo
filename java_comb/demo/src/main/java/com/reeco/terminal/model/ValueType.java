package com.reeco.terminal.model;

public enum ValueType {
    NUMBER,
    CATEGORICAL
}
