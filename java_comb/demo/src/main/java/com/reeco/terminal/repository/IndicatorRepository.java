package com.reeco.terminal.repository;

import java.util.List;

import com.reeco.terminal.model.Indicator;

import org.springframework.data.cassandra.repository.AllowFiltering;
import org.springframework.data.cassandra.repository.CassandraRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IndicatorRepository extends CassandraRepository<Indicator, Long> {
    
    @AllowFiltering
    List<Indicator> findByIndicatorId(Long indicatorId);

    long count();

}
