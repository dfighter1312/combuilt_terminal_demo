package com.reeco.terminal.repository;

import java.time.LocalDateTime;
import java.util.List;

import com.reeco.terminal.model.NumericSeries;

import org.springframework.data.cassandra.repository.CassandraRepository;
import org.springframework.data.cassandra.repository.Query;

public interface NumericRepository extends CassandraRepository<NumericSeries, Long> {

    @Query("SELECT * FROM numeric_series_by_organization WHERE indicator_id=:id LIMIT 10 ALLOW FILTERING")
    List<NumericSeries> findByIndicatorId(Long id);

    @Query("SELECT * FROM numeric_series_by_organization WHERE event_time <= :endEvent AND event_time >= :startEvent AND indicator_id = :id LIMIT 1 ALLOW FILTERING")
    List<NumericSeries> findByIndicatorIdAndDate(Long id, LocalDateTime startEvent, LocalDateTime endEvent);

}
