package com.reeco.terminal.service;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.reeco.terminal.model.Indicator;
import com.reeco.terminal.model.NumericSeries;
import com.reeco.terminal.model.Pair;
import com.reeco.terminal.repository.IndicatorRepository;
import com.reeco.terminal.repository.NumericRepository;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public class TerminalService {
    
    @Autowired
    private IndicatorRepository indicatorRepository;

    @Autowired
    private NumericRepository numericRepository;

    public ResponseEntity<List<Indicator>> getIndicator(Long id) {
        try {
            List<Indicator> _indicator = indicatorRepository.findByIndicatorId(id);
            if (_indicator.isEmpty()) {
                return new ResponseEntity<>(_indicator, HttpStatus.NO_CONTENT);
            }
            return new ResponseEntity<>(_indicator, HttpStatus.OK); 
        } catch (Exception e) {
            throw e;
        }
    }

    public ResponseEntity<Indicator> addIndicator(Indicator indicator) {
        try {
            indicator.setCreatedAt(LocalDateTime.now());
            indicator.setUpdatedAt(LocalDateTime.now());
            Indicator _indicator = indicatorRepository.insert(indicator);
            return new ResponseEntity<>(_indicator, HttpStatus.OK);
        } catch (Exception e) {
            throw e;
        }
    }

    public ResponseEntity<List<NumericSeries>> getSeries(Long id) {
        try {
            List<NumericSeries> _series = numericRepository.findByIndicatorId(id);
            if (_series.isEmpty()) {
                return new ResponseEntity<>(_series, HttpStatus.NO_CONTENT);
            }
            return new ResponseEntity<>(_series, HttpStatus.OK); 
        } catch (Exception e) {
            throw e;
        }
    }

    public ResponseEntity<NumericSeries> addSeries(NumericSeries numericSeries) {
        try {
            numericSeries.setReceivedAt(LocalDateTime.now());
            numericSeries.setEventTime(LocalDateTime.now());
            NumericSeries _series = numericRepository.insert(numericSeries);
            List<Pair> relatedParam = this.findParam(numericSeries.getIndicatorId());
            boolean derivedParamSuccess = this.addDerivedParam(relatedParam, LocalDateTime.now(), numericSeries);
            if (derivedParamSuccess) {
                return new ResponseEntity<>(_series, HttpStatus.OK);
            }
            return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public ResponseEntity<Integer> addNewParam(String name, Long id, Long param1, Long param2) {
        this.addIndicator(new Indicator(id, name));
        HashMap<String, String> paramInfo = new HashMap<String, String>();
        paramInfo.put("name", name);
        paramInfo.put("id", id.toString());
        paramInfo.put("param1", param1.toString());
        paramInfo.put("param2", param2.toString());
        JSONObject jsonObject = new JSONObject(paramInfo);
        try {
            FileWriter file = new FileWriter("params/" + name + ".json");
            file.write(jsonObject.toJSONString());
            file.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new ResponseEntity<>(1, HttpStatus.OK);
    }

    public List<Pair> findParam(Long long1) throws Exception {
        String param = Long.toString(long1);
        List<Pair> derivedParams = new ArrayList<Pair>();
        File folder = new File("params/");
        File[] fileList = folder.listFiles();
        JSONParser jsonParser = new JSONParser();
        for(File file: fileList) {
            try {
                String filename = "params/" + file.getName();
                FileReader reader = new FileReader(filename);
                JSONObject jsonObj = (JSONObject) jsonParser.parse(reader);

                String param1 = (String) jsonObj.get("param1");
                String param2 = (String) jsonObj.get("param2");

                if (param1.equals(param)) {
                    String newParam = (String) jsonObj.get("name");
                    Pair pair = new Pair(newParam, param2, "+");
                    derivedParams.add(pair);
                } else if (param2.equals(param)) {
                    String newParam = (String) jsonObj.get("name");
                    Pair pair = new Pair(newParam, param1, "+");
                    derivedParams.add(pair);
                };
                reader.close();
            } catch (Exception e) {
                throw e;
            }
        }
        return derivedParams;
    }

    public NumericSeries getSeriesByDate(Pair param, LocalDateTime date) {
        try {
            Long paramId = Long.parseLong(param.getValue());
            System.out.println(paramId);
            System.out.println(date.toString());
            List<NumericSeries> _series = numericRepository.findByIndicatorIdAndDate(
                paramId,
                date,
                date.minusMinutes(3)
            );
            System.out.println(_series.size());
            if (_series.isEmpty()) {
                return new NumericSeries();
            }
            return _series.get(_series.size() - 1); 
        } catch (Exception e) {
            throw e;
        }
    }

    public boolean addDerivedParam(List<Pair> relatedParam, LocalDateTime date, NumericSeries series1) {
        for (Pair pair: relatedParam) {
            NumericSeries series2 = this.getSeriesByDate(pair, date);
            if (series2.getValue() != null) {
                if (pair.getOpt() == "+") {
                    NumericSeries newParamSeries = new NumericSeries(
                        date,
                        pair.getKey(),
                        series1.getValue() + series2.getValue()
                    );
                    this.addSeries(newParamSeries);
                }
            }
        }
        return true;
    }
}