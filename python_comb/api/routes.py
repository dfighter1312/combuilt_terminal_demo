from http.client import HTTPException
from fastapi.routing import APIRouter
from model.indicator import Indicator, InsertIndicatorResponse, IndicatorOnDisplay, IndicatorOnInputUpdate, IndicatorOnSchedule
from model.query import QueryDataRequest
from utils.exceptions import InvalidInputException
from service.data_service import DataService
from starlette.status import (
    HTTP_201_CREATED, 
    HTTP_204_NO_CONTENT,
    HTTP_400_BAD_REQUEST,
    HTTP_200_OK
)

class Router(APIRouter):
    
    def __init__(self, *args, **kwargs) -> None:
        self.data_service = DataService()
        super(Router, self).__init__(
            tags=["combuild_param"],
            prefix="/v1",
            *args,
            **kwargs
        )
        self.setup()

    def setup(self):

        @self.post(
            path="/indicator/insert",
            name="Post: Insert new indicator",
            response_model=InsertIndicatorResponse
        )
        async def insert_indicator(request: Indicator):
            if not request:
                raise HTTPException(
                    status_code=HTTP_204_NO_CONTENT,
                    detail=f"{request} argument invalid"
                )
            try:
                return self.data_service.insert_indicator(request)
            except InvalidInputException as e:
                raise HTTPException(
                    status_code=HTTP_400_BAD_REQUEST,
                    detail=f"{e}"
                )

        @self.post(
            path="/create_dataset",
            name="Post: Get dataset",
        )
        async def create_dataset(request: QueryDataRequest):
            if not request:
                raise HTTPException(
                    status_code=HTTP_204_NO_CONTENT,
                    detail=f"{request} argument invalid"
                )
            try:
                return self.data_service.create_dataset(request)
            except InvalidInputException as e:
                raise HTTPException(
                    status_code=HTTP_400_BAD_REQUEST,
                    detail=f"{e}"
                )

        @self.post(
            path="/indicator/insert/on-input-change",
            name='Post: Insert indicator on input change'
        )
        async def insert_indicator_on_input_change(request: IndicatorOnInputUpdate):
            if not request:
                raise HTTPException(
                    status_code=HTTP_204_NO_CONTENT,
                    detail=f"{request} argument invalid"
                )
            try:
                return self.data_service.insert_indicator_on_input_change(request)
            except InvalidInputException as e:
                raise HTTPException(
                    status_code=HTTP_400_BAD_REQUEST,
                    detail=f"{e}"
                )

        @self.post(
            path="/indicator/insert/on-display",
            name='Post: Insert indicator on display'
        )
        async def insert_indicator_on_display(request: IndicatorOnDisplay):
            if not request:
                raise HTTPException(
                    status_code=HTTP_204_NO_CONTENT,
                    detail=f"{request} argument invalid"
                )
            try:
                return self.data_service.insert_indicator_on_display(request)
            except InvalidInputException as e:
                raise HTTPException(
                    status_code=HTTP_400_BAD_REQUEST,
                    detail=f"{e}"
                )

        @self.post(
            path="/indicator/insert/on-schedule",
            name='Post: Insert indicator on schedule'
        )
        async def insert_indicator_on_schedule(request: IndicatorOnSchedule):
            if not request:
                raise HTTPException(
                    status_code=HTTP_204_NO_CONTENT,
                    detail=f"{request} argument invalid"
                )
            try:
                return self.data_service.insert_indicator_on_schedule(request)
            except InvalidInputException as e:
                raise HTTPException(
                    status_code=HTTP_400_BAD_REQUEST,
                    detail=f"{e}"
                )