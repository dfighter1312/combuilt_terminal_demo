from logging import debug
from starlette.routing import Host
import uvicorn

if __name__ == "__main__":
    uvicorn.run("run:app", loop="none", port=8000, host="0.0.0.0")