from pydantic.dataclasses import dataclass
from datetime import date, datetime
from typing import Dict, List, Literal, Optional

from fastapi import Body

@dataclass
class Indicator:
    indicator_id: int
    equation: str
    indicator_name: Optional[str] = Body(None)
    indicator_name_vi: Optional[str] = Body(None)
    standard_unit: Optional[str] = Body(None)
    group_id: Optional[int] = Body(None)
    value_type: Literal['NUMBER', 'CATEGORICAL'] = 'NUMBER'
    created_at: Optional[datetime] = datetime.now()
    updated_at: Optional[datetime] = datetime.now()

@dataclass
class IndicatorOnSchedule:
    indicator: Indicator
    time_lag : str = "5min"
    weeks: int = 0
    days: int = 0
    hours: int = 0
    minutes: int = 0
    seconds: int = 0

@dataclass
class IndicatorOnInputUpdate:
    indicator: Indicator
    time_lag: str = "5min"

@dataclass
class IndicatorOnDisplay:
    indicator: Indicator
    time_lag: str = "5min"

@dataclass
class InsertIndicatorResponse:
    status_code: int
    paramNames: List[str]
    paramIds: List[int]
    errorSyntaxCount: int
    errorSyntaxMessages: List[str]
    unexistedParametersCount: int
    unexistedParametersMessage: Optional[str]
    runtimeErrorCount: int
    runtimeErrorMessage: Optional[str]
    validationPassed: bool
    insertSuccess: bool = True
    insertMessage: Optional[str] = None