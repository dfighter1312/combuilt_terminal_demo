from dataclasses import dataclass
from datetime import date
from typing import List


@dataclass
class QueryDataRequest:
    from_date: date
    to_date: date
    organization_id: int
    parameter: List[int]