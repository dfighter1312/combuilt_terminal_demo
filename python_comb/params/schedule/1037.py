import pandas as pd
import numpy as np

def f(data: pd.DataFrame):
	return data['Wind-speed'] + data['Wind-direction']

result = f(data)