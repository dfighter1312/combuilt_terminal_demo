import pandas as pd
import yaml
from cassandra.auth import PlainTextAuthProvider
from cassandra.cluster import Cluster
from cassandra.query import dict_factory
from loguru import logger
from model.query import QueryDataRequest
from model.indicator import Indicator
from utils.pivot import pivot_data
from utils.config import (
    CASSANDRA_HOST,
    CASSANDRA_KEYSPACE,
    CASSANDRA_PASSWORD,
    CASSANDRA_PORT,
    CASSANDRA_USERNAME,
    PARAMS_SCHEMA,
    NUMERIC_SERIES_SCHEMA,
    INDICATORS
)


class WarehouseRepository(object):
    def __init__(self) -> None:
        super().__init__()

    def _connect(self):
        auth_provider = PlainTextAuthProvider(username=CASSANDRA_USERNAME, password=CASSANDRA_PASSWORD)
        if not hasattr(self, "cluster"):
            try:
                logger.info("[FORECAST MODULE] Connecting to Cassandra database")
                self._cluster = Cluster(
                    auth_provider=auth_provider,
                    contact_points=[CASSANDRA_HOST],
                    port=CASSANDRA_PORT
                )
                self._session = self._cluster.connect()
                self._session.row_factory = dict_factory
                logger.info("[FORECAST MODULE] Connect success to Cassandra database")
                self._session.set_keyspace(CASSANDRA_KEYSPACE)
            except:
                logger.info("[FORECAST MODULE] Failed to connect")
                raise ConnectionError("Failed to connect to Cassandra database")

    def _disconnect(self):
        logger.info("[FORECAST MODULE] Disconnecting")
        self._cluster.shutdown()
        del self._cluster
        delattr(self,"_session")

    def create_dataset(self, request: QueryDataRequest):
        """Query data from request."""

        if not hasattr(self, "_session"):
            self._connect()

        # TODO: The query was modified for convenience
        # list_params = "('" + "', '".join(request.parameter) + "')"
        list_params = "(" + ",".join(str(x) for x in request.parameter) + ")"
        query = "SELECT organization_id, lat, lon, value, param_name, event_time FROM {0} WHERE date >= '{1}' AND date <= '{2}' AND param_id IN {3} AND organization_id={4} ALLOW FILTERING".format(
            NUMERIC_SERIES_SCHEMA,
            request.from_date.strftime("%Y-%m-%d"),
            request.to_date.strftime("%Y-%m-%d"),
            list_params,
            request.organization_id
        )
        print(query)
        result = self._session.execute(query).all()
        logger.info("[FORECAST MODULE] Querying: " + str(query))
        self._disconnect()
        result = pd.DataFrame(result)
        result = pivot_data(result)
        return result
    
    def get_params_by_org(self, organization_id: int, return_type: str='tuples'):
        """Get available parameters of a specific station

        Args:
            organization_id (int): Station ID
        """
        if not hasattr(self, "_session"):
            self._connect()

        query = "SELECT param_id, param_name FROM {0} WHERE organization_id = {1} ALLOW FILTERING".format(
            PARAMS_SCHEMA,
            organization_id
        )
        param_name = self._session.execute(query).all()
        if return_type == 'tuples':
            result = list(map(lambda x: (x["param_id"], x["param_name"]), param_name))
        if return_type == 'dict':
            result = dict()
            for i in param_name:
                result[i["param_name"]] = i["param_id"]
        self._disconnect()
        return result

    def insert_indicator(self, request: Indicator):
        if not hasattr(self, "_session"):
            self._connect()
        # Note of using %s for every parameter, not just strings
        cql_stmt = "INSERT INTO {0} (indicator_id, indicator_name, indicator_name_vi, standard_unit, value_type, group_id, created_at, updated_at) VALUES (%s, %s, %s, %s, %s, %s, %s, %s)".format(INDICATORS)
        results = self._session.execute(
            cql_stmt,
            (
                request.indicator_id,
                request.indicator_name,
                request.indicator_name_vi,
                request.standard_unit,
                request.value_type,
                request.group_id,
                request.created_at,
                request.updated_at
            )
        )
        self._disconnect()
        return results

    def insert_param_values(self, values):
        pass