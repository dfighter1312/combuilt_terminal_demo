import logging

from loguru import logger
from scheduling.schedule import ParamScheduler
from utils.logging import InterceptHandler
from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
from api.routes import Router


class APIModule(FastAPI):
    """Creates an application instance

    ** Possible parameters**
    Due to being inherited from FastAPI library. Almost all
    paramters are identical with FastAPI.

    Ref: https://github.com/tiangolo/fastapi
    """

    def __init__(self, *args, **kwargs):
        self.scheduler = ParamScheduler()
        super(APIModule, self).__init__(*args, **kwargs)

    def get_application(self):
        """ 
        
        Prepare the main application. Taking responsibility for:
        1. Connect to API router (define api prefix, tags,...)
        2. Setup logging function (logging level, log rotation, retention policy, ...)
        
        """
        router = Router()
        self.include_router(router, prefix="/api")

        self.add_middleware(
            CORSMiddleware, allow_origins=["*"], allow_credentials=True, allow_methods=["*"], allow_headers=["*"],
        )

        LOGGING_LEVEL = logging.DEBUG if self.debug else logging.INFO

        logging.basicConfig(
            handlers=[InterceptHandler(level=LOGGING_LEVEL)], level=LOGGING_LEVEL,
        )

        logger.configure(
            handlers=[{"sink": "logs/forecasting-module.log", "level": LOGGING_LEVEL, "rotation": "5 MB",}]
        )

        self.scheduler.start()

        return self

init = APIModule()
app = init.get_application()