import atexit
from datetime import datetime, timedelta
from apscheduler.schedulers.background import BackgroundScheduler
from apscheduler.triggers.interval import IntervalTrigger

scheduler = BackgroundScheduler()

class ParamScheduler:

    def __init__(self):
        pass

    def run():
        print("The function is running at %s" % datetime.now())

    def run_another():
        print("The function is launched at %s" % datetime.now())

    def start(self):
        scheduler.start()
        atexit.register(lambda: scheduler.shutdown(wait=False))

    def add_job(self, f, start_date, weeks=0, days=0, hours=0, minutes=0, seconds=0):
        job = scheduler.add_job(
            f,
            trigger=IntervalTrigger(
                start_date=start_date,
                weeks=weeks,
                days=days,
                hours=hours,
                minutes=minutes,
                seconds=seconds
            )
        )
        return job.id

    def remove_job(self, job_id):
        scheduler.remove_job(job_id)