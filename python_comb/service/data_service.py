import io

from torch import isin
from model.query import QueryDataRequest
from model.indicator import Indicator, InsertIndicatorResponse, IndicatorOnDisplay, IndicatorOnInputUpdate, IndicatorOnSchedule
from fastapi.responses import StreamingResponse
from utils.validator import Validator
from utils.pivot import pivot_data
from utils.derived_param import DerivedParamGeneration
from repository.cassandra import WarehouseRepository


class DataService:

    def __init__(self) -> None:
        self.repository = WarehouseRepository()
        self.derived_processor = DerivedParamGeneration()
        self.validator = Validator()

    def create_dataset(self, request: QueryDataRequest):
        # TODO: Currently, only derived params are considered
        for p in request.parameter:
            param_dict = self.derived_processor.get_param_dict(p)
            request_clone = request
            request_clone.parameter = param_dict["param_lst"]
            data = self.repository.create_dataset(request_clone)
            data = pivot_data(data)
            data.set_index("event_time", inplace=True)
            indices = data.iloc[:, 0].dropna().index
            data = data.apply(lambda x: x.dropna().reindex(
                indices,
                method='nearest',
                tolerance=param_dict["time_lag"]
            ))
            locals = {}
            exec(param_dict["content"], {"data": data}, locals)
            result = locals["result"]
            result = result.rename(param_dict["param_name"])
            result = result.to_frame()
            result["event_time"] = data.index
        stream = io.StringIO()
        result.to_csv(stream, index=False)
        response = StreamingResponse(
            iter([stream.getvalue()]),
            media_type="text/csv"
        )
        response.headers["Content-Disposition"] = "attachment; filename={}".format(str(param_dict["param_id"]) + ".csv")
        response._headers["Access-Control-Expose-Headers"]="Content-Disposition"
        return response

    def insert_indicator(self, type: int, request: Indicator, **kwargs):
        val_obj = self.validator.validate(request.equation)
        if val_obj["validationPassed"]:
            try:
                self.derived_processor.create_param_internal_files(request, val_obj["paramIds"], type, **kwargs)
                self.repository.insert_indicator(request)
            except (Exception, FileExistsError) as e:
                if isinstance(e, FileExistsError):
                    val_obj["insertSuccess"] = False
                    val_obj["insertMessage"] = "Parameter already exists"
                else:
                    self.derived_processor.remove_file(request.indicator_id, type)
                    val_obj["insertSuccess"] = False
                    val_obj["insertMessage"] = e.__class__.__name__ + ': ' + e.args[0]
        return InsertIndicatorResponse(200, **val_obj)

    def insert_indicator_on_input_change(self, request: IndicatorOnInputUpdate):
        return self.insert_indicator(
            "input_update",
            request.indicator,
            time_lag=request.time_lag
        )

    def insert_indicator_on_schedule(self, request: IndicatorOnSchedule):
        return self.insert_indicator(
            "schedule",
            request.indicator,
            time_lag=request.time_lag,
            weeks=request.weeks,
            days=request.days,
            hours=request.hours,
            minutes=request.minutes,
            seconds=request.seconds
        )

    def insert_indicator_on_display(self, request: IndicatorOnDisplay):
        return self.insert_indicator(
            "display",
            request.indicator,
            time_lag=request.time_lag
        )