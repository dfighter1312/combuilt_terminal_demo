from typing import Dict, List, Set
import re
import numpy as np
import pandas as pd


def syntax_checker(code: str, start_line: int = 0):
    """Check syntax of the code"""
    errors = []
    try:
        code = code.replace('return ', '')
        codeObj = compile(code, 'code', 'exec')
    except SyntaxError as e:
        args = e.args
        err_dict = {
            "error_type": e.__class__.__name__,
            "comment": args[0],
            "line": args[1][1] + start_line,
            "pos": args[1][2],
            "code": args[1][3]
        }
        errors.append(err_dict)
        errors += syntax_checker(
            "\n".join(code.split("\n")[err_dict["line"]:]),
            err_dict["line"]
        )
    return errors

def scan_parameter(equation):
    params = re.findall(
        pattern=r"data\[\'[\w\s]+\'\]",
        string=equation
    )
    params += re.findall(
        pattern=r"data\[\"[\w\s]+\"\]",
        string=equation
    )
    params = set([p[6:-2] for p in params])
    return params

def get_parameters():
    return set(["New", "Old", "Bad", "Good"])

def check_exists(a: Set, b: Set):
    return a.difference(b)

def create_execution_file(content) -> str:
    full_content = ""
    with open('template/header.py', 'r') as header:
        full_content = header.read()

    content = content.split("\n")
    content = "\t" + "\n\t".join(content) + "\n"
    full_content += content
    full_content += "\n"

    with open('template/footer.py', 'r') as footer:
        full_content += footer.read()

    return full_content

def runtime_checker(code, parameters):
    sample_num = 5
    sample_data = np.random.rand(sample_num, len(parameters))
    data = pd.DataFrame(sample_data, columns=parameters)
    locals = {}
    try:
        exec(code, {"data": data}, locals)
        if locals["result"] is None:
            return {
                "error_type": "NoReturnFound",
                "comment": "There is no 'return' statement in your code or in any output branch."
            }
        if not isinstance(locals["result"], pd.Series):
            return {
                "error_type": "InvalidReturnType",
                "comment": "Need Series (single column) but {} found".format(
                    type(locals["result"]).__name__
                ), 
            }
        if len(locals["result"]) != sample_num:
            return {
                "error_type": "IncompatibleReturn",
                "comment": "Returned Series is not aligned with original Series. Check if you accidentally insert/delete any row."
            }
    except Exception as e:
        args = e.args
        return {
            "error_type": e.__class__.__name__,
            "comment": args[0]
        }
    return None

def log_syntax_errors(errors: List[Dict]):
    for e in errors:
        print("{}: {} at line {}".format(
            e["error_type"],
            e["comment"],
            e["line"]
        ))
        print("   ", e["code"].replace('\n', ''))
        print("^".rjust(e["pos"] + 4))

def log_not_exists_param_errors(errors: List[str]):
    print("ParamNotFoundError:", ', '.join('"{}"'.format(p) for p in errors), "not exist(s)")

def log_runtime_errors(errors: Dict):
    print("{}: {}".format(errors["error_type"], errors["comment"]))

if __name__ == '__main__':
    # code = 'return data["New"34] + data["Old"]\ndata[15"New"] * 2\n2 + 2 == 4' # Code with syntax error
    # code = 'return data["New"] * data[Old"]' # Code with syntax error
    # code = 'return data["New"] + data["Not known"] - data[\'Unknown\']' # Code with ParameterNotFoundError
    # code = 'data["New"] + data["Old"]' # Code with RuntimeError (no return)
    # code = 'b = 2\nreturn data["New"] * b + datum["Old"]' # Code with RuntimeError (NameError)
    # code = 'new = data["New"] + data["Good"]\nnewer = new * data["Bad"]\nnewest = worst + new' # Code with RuntimeError (NameError)
    # code = 'return (data["New"] + data["Old"]).sum()' # Code with RuntimeError (InvalidReturnType)
    # code = 'newParam = data["New"] + data["Old"]\nreturn newParam[1:]' # Code with RuntimeError (IncompatibleReturn)
    code = 'return data["New"] + data["Old"]' # Correct code, nothing is logged

    # Syntax check
    errors = syntax_checker(code)
    if len(errors) != 0:
        log_syntax_errors(errors)
        exit()

    # Parameter validation
    params = scan_parameter(code)
    exist_params = get_parameters()
    not_exist_params = check_exists(params, exist_params)
    if len(not_exist_params) != 0:
        log_not_exists_param_errors(not_exist_params)
        exit()

    # Runtime check
    file_full_content = create_execution_file(code)
    runtime_err = runtime_checker(file_full_content, params)
    if runtime_err is not None:
        log_runtime_errors(runtime_err)
        exit()

