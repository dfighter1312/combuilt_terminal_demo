import logging
import sys
from typing import Dict, List, Optional

import yaml
from loguru import logger

config_file = "config.yml"
with open(config_file, "r") as f:
    config = yaml.load(f, Loader=yaml.FullLoader)


databases_config = config["databases"]
cassandra_config = databases_config["cassandra"]
mongo_config = databases_config["mongo"]
local_file_config = config["localfile"]

CASSANDRA_HOST: str = cassandra_config["host"]
CASSANDRA_PORT: int = cassandra_config["port"]
CASSANDRA_USERNAME: str = cassandra_config["username"]
CASSANDRA_PASSWORD: str = cassandra_config["password"]
CASSANDRA_KEYSPACE: str = cassandra_config["keyspace"]
PARAMS_SCHEMA: str = cassandra_config["schema"]["params_by_organization"]
NUMERIC_SERIES_SCHEMA: str = cassandra_config["schema"]["numeric_series_by_organization"]
INDICATORS: str = cassandra_config["schema"]["indicators"]


MONGO_HOST: str = mongo_config["host"]
MONGO_PORT: int = mongo_config["port"]

LOCAL_DATA_PATH : str = local_file_config["path"]["data"]
LOCAL_PIPELINE_PATH : str = local_file_config["path"]["pipeline"]
LOCAL_LOG_PATH : str = local_file_config["path"]["log"]
LOCAL_MODEL_CONFIG_PATH : str = local_file_config["path"]["model_config"]

APP_NAME: str = config["app_name"]