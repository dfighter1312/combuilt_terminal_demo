import os
import re
from typing import List
import numpy as np
import yaml

from model.indicator import Indicator


class DerivedParamGeneration:

    def create_execution_file(self, param_id, content, update_type) -> str:
        full_content = ""
        with open('template/header.py', 'r') as header:
            full_content = header.read()
        
        content = content.split("\n")
        content = "\t" + "\n\t".join(content) + "\n"
        full_content += content
        full_content += "\n"
        
        with open('template/footer.py', 'r') as footer:
            full_content += footer.read()

        filename = f"params/{update_type}/{param_id}.py"
        with open(filename, 'x') as p:
            p.write(full_content)
        
        return filename

    def remove_file(self, param_id, update_type):
        py_path = f"params/{update_type}/{param_id}.py"
        yml_path = f"params/{update_type}/{param_id}.yml"
        if os.path.exists(py_path):
            os.remove(py_path)
        if os.path.exists(yml_path):
            os.remove(yml_path)

    def create_metadata(self, param_name, param_id, param_lst, update_type, **kwargs):
        metadata_path = f"params/{update_type}/{param_id}.yml"
        print(metadata_path)
        eq_path = f"params/{update_type}/{param_id}.py"
        d = {
            "param_name": param_name,
            "param_id": param_id,
            "param_lst": param_lst,
            "metadata_path": metadata_path,
            "eq_path": eq_path
        }
        d.update(**kwargs)
        with open(metadata_path, 'x') as f:
            yaml.dump(d, f, indent=4)
        return metadata_path

    def get_param_dict(self, param_id, update_type):
        filepath = f"params/{update_type}/{param_id}.yml"
        with open(filepath, 'r') as f:
            param_dict = yaml.load(f, Loader=yaml.FullLoader)
        with open(param_dict["eq_path"], 'r') as p:
            param_dict["content"] = p.read()
        return param_dict

    def create_param_internal_files(self, request: Indicator, params: List, update_type: str, **kwargs):
        self.create_execution_file(
            request.indicator_id,
            request.equation,
            update_type
        )
        self.create_metadata(
            request.indicator_name,
            request.indicator_id,
            params,
            update_type,
            **kwargs
        )