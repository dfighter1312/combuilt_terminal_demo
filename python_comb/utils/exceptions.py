class InvalidInputException(BaseException):
    """Raised when the data input does not meet predefined constraints"""

    ...