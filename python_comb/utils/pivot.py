def pivot_data(df):
    """Pivot the data which is retrieved from the database."""
    try:
        df_unmelted = df.pivot(
            values='value',
            columns='param_name',
            index=['event_time', 'organization_id', 'lat', 'lon']
        ).reset_index()
        return df_unmelted
    except:
        return df
