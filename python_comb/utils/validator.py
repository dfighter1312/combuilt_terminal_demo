import re
from typing import Dict, List, Set
import numpy as np

import pandas as pd

from repository.cassandra import WarehouseRepository


class Validator:

    def __init__(self) -> None:
        self.repository = WarehouseRepository()

    def syntax_check(self, code: str, start_line: int = 0):
        """Check syntax of the code"""
        errors = []
        try:
            code = code.replace('return ', '')
            compile(code, 'code', 'exec')
        except SyntaxError as e:
            args = e.args
            err_dict = {
                "error_type": e.__class__.__name__,
                "comment": args[0],
                "line": args[1][1] + start_line,
                "pos": args[1][2],
                "code": args[1][3]
            }
            errors.append(err_dict)
            errors += self.syntax_checker(
                "\n".join(code.split("\n")[err_dict["line"]:]),
                err_dict["line"]
            )
        return errors

    def scan_parameter(self, equation):
        params = re.findall(
            pattern=r"data\[\'[^\']+\'\]",
            string=equation
        )
        params += re.findall(
            pattern=r"data\[\"[^\"]+\"\]",
            string=equation
        )
        params = set([p[6:-2] for p in params])
        return params

    def get_parameters(self):
        return self.repository.get_params_by_org(9, 'dict')

    def check_exists(self, a: Set, b: Set):
        return a.difference(b)

    def create_execution_text(self, content) -> str:
        full_content = ""
        with open('template/header.py', 'r') as header:
            full_content = header.read()

        content = content.split("\n")
        content = "\t" + "\n\t".join(content) + "\n"
        full_content += content
        full_content += "\n"

        with open('template/footer.py', 'r') as footer:
            full_content += footer.read()

        return full_content

    def runtime_checker(self, code, parameters):
        sample_num = 5
        sample_data = np.random.rand(sample_num, len(parameters))
        data = pd.DataFrame(sample_data, columns=parameters)
        locals = {}
        try:
            exec(code, {"data": data}, locals)
            if locals["result"] is None:
                return {
                    "error_type": "NoReturnStatement",
                    "comment": "There is no 'return' statement in your code or in any output branch."
                }
            if not isinstance(locals["result"], pd.Series):
                return {
                    "error_type": "InvalidReturnType",
                    "comment": "Need Series (single column) but {} found".format(
                        type(locals["result"]).__name__
                    ), 
                }
            if len(locals["result"]) != sample_num:
                return {
                    "error_type": "IncompatibleReturn",
                    "comment": "Returned Series is not aligned with original Series. Check if you accidentally insert/delete any row."
                }
        except Exception as e:
            args = e.args
            return {
                "error_type": e.__class__.__name__,
                "comment": args[0]
            }
        return None

    def get_syntax_error_log(self, errors: List[Dict]):
        messages = []
        for e in errors:
            m = ["{}: {} at line {}, position {}".format(e["error_type"], e["comment"], e["line"], e["pos"])]
            m += "   ", e["code"].replace('\n', '')
            m += "^".rjust(e["pos"] + 4)
            messages.append(m)
        return messages

    def get_not_exists_param_error_log(self, errors: List[str]):
        return "ParamNotFoundError:" + ', '.join('"{}"'.format(p) for p in errors) + "not exist(s)"

    def get_runtime_error_log(self, errors: Dict):
        return "{}: {}".format(errors["error_type"], errors["comment"])

    def validate(self, code):
        val_obj = {
            "paramNames": list(),
            "paramIds": list(),
            "errorSyntaxCount": 0,
            "errorSyntaxMessages": list(),
            "unexistedParametersCount": 0,
            "unexistedParametersMessage": None,
            "runtimeErrorCount": 0,
            "runtimeErrorMessage": None,
            "validationPassed": True
        }
        # Syntax check
        errors = self.syntax_check(code)
        if len(errors) != 0:
            val_obj["errorSyntaxCount"] = len(errors)
            val_obj["errorSyntaxMessages"] = self.get_syntax_error_log(errors)
            val_obj["validationPassed"] = False

        # Parameter validation
        params = self.scan_parameter(code)
        exist_params = self.get_parameters()
        not_exist_params = self.check_exists(params, set(exist_params.keys()))
        if len(not_exist_params) != 0:
            val_obj["unexistedParametersCount"] = len(not_exist_params)
            val_obj["unexistedParametersMessage"] = self.get_not_exists_param_error_log(not_exist_params)
            val_obj["validationPassed"] = False
        else:
            val_obj["paramNames"] = params
            val_obj["paramIds"] = [exist_params[p] for p in params]

        # Runtime check
        file_full_content = self.create_execution_text(code)
        runtime_err = self.runtime_checker(file_full_content, params)
        if runtime_err is not None:
            val_obj["runtimeErrorCount"] = 1
            val_obj["runtimeErrorMessage"] = self.get_runtime_error_log(runtime_err)
            val_obj["validationPassed"] = False
        return val_obj