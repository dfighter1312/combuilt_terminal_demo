from kafka import KafkaClient

from read_config import read_config

config = read_config()
client = KafkaClient(
    bootstrap_servers=[config['server']]
)
print(client.bootstrap_connected())
lln = client.least_loaded_node()
print(client.connected(lln))
print(client.ready(lln))
