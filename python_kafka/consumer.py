from kafka import KafkaConsumer
from json import loads

from read_config import read_config

if __name__ == '__main__':
    config = read_config()
    consumer = KafkaConsumer(
        config['topic_name'],
        bootstrap_servers=config['server']
    )
    for message in consumer:
        print(message.value)
