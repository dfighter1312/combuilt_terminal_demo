# To use kafka, type command
# pip install kafka-python

from datetime import datetime
import pprint
import time
from kafka import KafkaProducer
from read_config import read_config
import pprint
import json
import yaml
import logging

logging.basicConfig(
    level=logging.DEBUG,
    filename='log.txt',
    filemode='w',
    format= '[%(asctime)s] %(levelname)s - %(message)s',
    datefmt='%H:%M:%S'
)

if __name__ == "__main__":
    config = read_config()
    producer = KafkaProducer(
        bootstrap_servers=[config['server']],
        value_serializer=lambda x: json.dumps(x).encode('utf-8'),
        # request_timeout_ms=2000,
    )

    for i in range(5):
        sent_object = {
            "organizationId": 1,
            "workspaceId": 1,
            "stationId": 1,
            "connectionId": 3,
            "paramId": 36,
            "eventTime": "2021-11-11 09:40:00.000",
            "indicatorId": 1,
            "indicatorName": "Hello there",
            "paramName": "Alo-123",
            "value": "25",
            "receivedAt": "2022-04-01 15:39:37.111",
            "sentAt": "2022-04-01 15:39:37.111",
            "lat": 25.0,
            "lon": 25.0}
        # If putting a wrong topic name, this will get a message stating that the metadata cannot be retrieved
        # Otherwise, it print a Timeout error after 60 secs
        record_metadata = producer.send(
            config['topic_name'],
            sent_object,
            partition=0,
        )
        producer.flush()
        print(record_metadata.get())
    
    producer.close()