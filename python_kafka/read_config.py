import yaml

config_file = 'config.yml'

def read_config():
    with open(config_file, "r") as f:
        config = yaml.load(f, Loader=yaml.FullLoader)
    return config