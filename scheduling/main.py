import atexit
import time
from datetime import datetime, timedelta
from apscheduler.schedulers.background import BackgroundScheduler
from apscheduler.triggers.interval import IntervalTrigger

scheduler = BackgroundScheduler()
scheduled_time = datetime.now().__add__(timedelta(seconds=1))
# @scheduler.scheduled_job(IntervalTrigger(seconds=3, start_date=scheduled_time))
def run(x):
    print("The function {} is running at {}".format(x, datetime.now()))

# @scheduler.scheduled_job(IntervalTrigger(seconds=15, start_date=scheduled_time))
def run_another(y):
    print("The function {} is launched at {}".format(y, datetime.now()))

print("Now is %s" % datetime.now())

scheduler.add_job(
    lambda: run(2),
    IntervalTrigger(
        seconds=3,
        start_date=scheduled_time
    )
)
scheduler.add_job(
    lambda: run_another(3),
    IntervalTrigger(
        seconds=5,
        start_date=scheduled_time
    )
)
scheduler.start()
time.sleep(100)
atexit.register(lambda: scheduler.shutdown(wait=False))